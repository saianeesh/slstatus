# slstatus

Slightly customised version of slstatus with added:
- CPU usage (%)
- RAM usage (%)
- Free disk space (Mi/Gi)
- Net usage (Bytes default, changes according to usage Ki/Mi)
- Date/time (dd-mm hh:mm:ss AM/PM)
- Added font-awesome icons and text support

Optional dependancies:
- ttf-font-awesome (for icon support. If you would like to change the icons check _"https://fontawesome.com/cheatsheet"_)
